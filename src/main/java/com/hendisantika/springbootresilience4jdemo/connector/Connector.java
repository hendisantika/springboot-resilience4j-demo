package com.hendisantika.springbootresilience4jdemo.connector;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.concurrent.CompletableFuture;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-resilience4j-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 10/11/19
 * Time: 07.41
 */
public interface Connector {
    String failure();

    String success();

    String successException();

    String ignoreException();

    Flux<String> fluxFailure();

    Mono<String> monoSuccess();

    Mono<String> monoFailure();

    Flux<String> fluxSuccess();

    String failureWithFallback();

    CompletableFuture<String> futureSuccess();

    CompletableFuture<String> futureFailure();

}