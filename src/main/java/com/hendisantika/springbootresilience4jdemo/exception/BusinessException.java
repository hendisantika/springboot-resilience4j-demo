package com.hendisantika.springbootresilience4jdemo.exception;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-resilience4j-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 10/11/19
 * Time: 07.43
 */
public class BusinessException extends RuntimeException {

    public BusinessException(String message) {
        super(message);
    }
}