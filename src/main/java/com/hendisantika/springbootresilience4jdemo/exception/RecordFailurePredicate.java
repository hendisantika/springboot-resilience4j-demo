package com.hendisantika.springbootresilience4jdemo.exception;

import java.util.function.Predicate;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-resilience4j-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 10/11/19
 * Time: 07.43
 */
public class RecordFailurePredicate implements Predicate<Throwable> {
    @Override
    public boolean test(Throwable throwable) {
        return !(throwable instanceof BusinessException);
    }
}