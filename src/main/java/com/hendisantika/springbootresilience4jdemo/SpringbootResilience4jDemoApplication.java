package com.hendisantika.springbootresilience4jdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootResilience4jDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootResilience4jDemoApplication.class, args);
    }

}
